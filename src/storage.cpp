#include "storage.h"
#include "util.h"

Storage::Storage(QObject *parent,
                 Options const &options)
  : QObject{parent}
  , m_options{options}
{
}

Storage::~Storage() {
}

bool
Storage::openRead() {
  error(Q("Storage: reading from the selected storage backend is not supported"));
  return false;
}

std::pair<FlowData, bool>
Storage::read() {
  error(Q("Storage: reading from the selected storage backend is not supported"));
  return std::make_pair(FlowData{}, false);
}
