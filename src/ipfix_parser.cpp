#include <QByteArray>
#include <QDateTime>
#include <QStringList>

#include "ipfix_parser.h"
#include "memory_stream.h"
#include "storage.h"
#include "util.h"

IpfixParser::Template::Template()
  : expectedFlowSize{}
{
}

void
IpfixParser::Template::reset(unsigned int reserveFieldStorage) {
  expectedFlowSize = 0;
  fields.clear();
  fields.reserve(reserveFieldStorage);
}

void
IpfixParser::Template::addField(TemplateField const &field) {
  fields << field;
  expectedFlowSize += field.length;
}

// ----------------------------------------------------------------------

IpfixParser::IpfixParser(Options const &options)
  : m_options{options}
  , m_observationDomainID{}
  , m_flowSequence{}
  , m_expectedNextFlowSequence{}
  , m_datagramTimestamp{}
{
}

void
IpfixParser::parseDatagram(MemoryStream datagram) {
  try {
    doParseDatagram(datagram);
  } catch (MemoryStream::EndOfStreamX const &) {
  }
}

void
IpfixParser::doParseDatagram(MemoryStream datagram) {
  auto version          = datagram.getUInt16BE();
  auto length           = datagram.getUInt16BE();
  m_datagramTimestamp   = datagram.getUInt32BE() * 1000ull;
  m_flowSequence        = datagram.getUInt32BE();
  m_observationDomainID = datagram.getUInt32BE();

  if (version != 10) {
    debug(Q("ipfix: version %1 != 10").arg(version));
    return;
  }

  if ((-1 != m_options.observationDomainID) && (m_observationDomainID != static_cast<unsigned int>(m_options.observationDomainID)))
    return;

  debug(Q("  ipfix: version %1 length %2 timestamp %3 flowSequence %4 obsDomID %5")
        .arg(version)
        .arg(length)
        .arg(QDateTime::fromMSecsSinceEpoch(m_datagramTimestamp).toString("yyyy-MM-dd HH:mm:ss"))
        .arg(m_flowSequence)
        .arg(m_observationDomainID));

  while (!datagram.isEmpty()) {
    auto bytesUsed = parseSet(datagram);
    if (0 == bytesUsed) {
      debug(Q("   Error in data stream, set parsing returned 0, aborting datagram"));
      return;
    }

    datagram.skip(bytesUsed);
  }
  for (auto const &data : m_dataToAccount)
    m_options.storage->write(data);

  m_dataToAccount.clear();
}

size_t
IpfixParser::parseSet(MemoryStream mem) {
  if (mem.getRemainingBytes() < 4)
    return mem.getRemainingBytes();

  auto setID     = mem.getUInt16BE();
  auto setLength = mem.getUInt16BE();

  try {
    if (setID == TemplateSetID)
      parseTemplateSet(mem.slice(setLength - 4));

    else if (setID >= MinimumDataSetID)
      parseDataSet(setID, mem.slice(setLength - 4));

  } catch (MemoryStream::EndOfStreamX const &ex) {
    debug(Q("   Error in data stream: end of data encountered, need %2 bytes; maybe invalid template for current data set ID %1?").arg(setID).arg(ex.m_missingBytes));
  }

  return setLength;
}

void
IpfixParser::parseTemplateSet(MemoryStream mem) {
  while (!mem.isEmpty()) {
    auto bytesUsed = parseTemplate(mem.slice(mem.getRemainingBytes()));
    mem.skip(bytesUsed);
  }
}

size_t
IpfixParser::parseTemplate(MemoryStream mem) {
  auto templateID       = mem.getUInt16BE();
  auto fieldCount       = mem.getUInt16BE();
  auto actualFieldCount = 0u;

  if (templateID == TemplateSetID) {
    debug(Q("    Templates: clearing all"));
    m_templates.clear();
    return mem.getSize();
  }

  auto &flowTemplate = m_templates[templateID];

  flowTemplate.reset(fieldCount);

  while (actualFieldCount < fieldCount) {
    auto value       = mem.getUInt16BE();
    auto penProvided = !!(value & 0x8000);
    auto fieldLength = mem.getUInt16BE();
    auto pen         = penProvided ? mem.getUInt32BE() : 0u;

    flowTemplate.addField(TemplateField{static_cast<TemplateType>(value & 0x7fffu), fieldLength, pen});

    ++actualFieldCount;
  }

  if (actualFieldCount == 0) {
    m_templates.remove(templateID);
    debug(Q("    Templates: remove templateID %1").arg(templateID));

  } else {
    debug(Q("    Templates: add/refresh templateID %1 fieldCount %2 actualFieldCount %3 bytesUsed %4 expectedFlowSize %5")
          .arg(templateID).arg(fieldCount).arg(actualFieldCount).arg(mem.getSize() - mem.getRemainingBytes()).arg(flowTemplate.expectedFlowSize));

    auto fieldNum = 0;
    for (auto const &fieldTemplate : flowTemplate.fields)
      debug(Q("      field %1 type %2 length %3 PEN %4").arg(++fieldNum).arg(fieldTemplate.type, 4, 16, QLatin1Char('0')).arg(fieldTemplate.length).arg(fieldTemplate.pen));
  }

  return mem.getSize() - mem.getRemainingBytes();
}

void
IpfixParser::parseDataSet(uint16_t setID,
                          MemoryStream mem) {
  if (!m_templates.contains(setID)) {
    debug(Q("    data set %1: no template known").arg(setID));
    return;
  }

  auto const &flowTemplate = m_templates[setID];
  auto flowNum             = 0;

  while (!mem.isEmpty()) {
    // If there's less data available than the flow needs according to
    // its template then this is supposed to be padding. Check if all
    // padding bytes are actually 0x00.
    auto remainingBytes = mem.getRemainingBytes();
    if (remainingBytes < flowTemplate.expectedFlowSize) {
      while (!mem.isEmpty()) {
        auto byte = mem.getUInt8();
        if (byte) {
          debug(Q("      Error in data stream: found padding byte != 0x00 in trailing data in data set ID %1; expectedFlowSize %2 remainingBytes %3").arg(setID).arg(flowTemplate.expectedFlowSize).arg(remainingBytes));
          return;
        }
      }

      return;
    }

    // We have enough data left in the packet. Parse the flow.
    ++flowNum;

    FlowData data{};
    data.m_timestamp = m_datagramTimestamp;

    for (auto const &fieldTemplate : flowTemplate.fields)
      parseField(data, mem, fieldTemplate);

    if (!data.isValid())
      continue;

    if ((m_options.ipv4Only && !data.isIPv4()) || (m_options.ipv6Only && !data.isIPv6()))
      continue;

    debug(Q("    Data parsed for set %1 flow num %2: %3").arg(setID).arg(flowNum).arg(data.toString()));
    data.m_observationDomainID = m_observationDomainID;
    m_dataToAccount << data;
  }
}

void
IpfixParser::parseField(FlowData &data,
                        MemoryStream &mem,
                        TemplateField const &fieldTemplate) {
  if (fieldTemplate.pen)
    mem.skip(fieldTemplate.length);

  else if (fieldTemplate.type == TypeSrcAddr) {
    if (fieldTemplate.length == 4)
      data.m_srcAddress.readIPv4From(mem);
    else
      data.m_srcAddress.readIPv6From(mem);

  } else if (fieldTemplate.type == TypeDstAddr) {
    if (fieldTemplate.length == 4)
      data.m_dstAddress.readIPv4From(mem);
    else
      data.m_dstAddress.readIPv6From(mem);

  } else if (fieldTemplate.type == TypeIPv4SrcAddr)
    data.m_srcAddress.readIPv4From(mem);

  else if (fieldTemplate.type == TypeIPv4DstAddr)
    data.m_dstAddress.readIPv4From(mem);

  else if (fieldTemplate.type == TypeLayer4SrcPort)
    data.m_srcPort = mem.getUIntBE(fieldTemplate.length);

  else if (fieldTemplate.type == TypeLayer4DstPort)
    data.m_dstPort = mem.getUIntBE(fieldTemplate.length);

  else if (fieldTemplate.type == TypeProtocol)
    data.m_protocol = mem.getUIntBE(fieldTemplate.length);

  else if (fieldTemplate.type == TypePackets)
    data.m_packets = mem.getUIntBE(fieldTemplate.length);

  else if (fieldTemplate.type == TypeBytes)
    data.m_bytes = mem.getUIntBE(fieldTemplate.length);

  else
    mem.skip(fieldTemplate.length);
}
