#include <QCoreApplication>
#include <QSocketNotifier>

#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>

#include "memory_stream.h"
#include "server.h"
#include "util.h"

int Server::ms_sigIntFD[2];

Server::Server(QObject *parent,
               Options const &options)
  : QObject{parent}
  , m_options{options}
  , m_socket{this}
  , m_parser{options}
  , m_sigIntNotifier{}
{
  if (::socketpair(AF_UNIX, SOCK_STREAM, 0, ms_sigIntFD))
    error(Q("Couldn't create INT socketpair"));

  m_sigIntNotifier = new QSocketNotifier{ms_sigIntFD[1], QSocketNotifier::Read, this};

  connect(m_sigIntNotifier, SIGNAL(activated(int)), this, SLOT(handleSigInt()));
  connect(&m_socket,        SIGNAL(readyRead()),    this, SLOT(readPendingDatagrams()));
}

void
Server::initialize() {
  struct sigaction saInt;

  saInt.sa_handler = Server::intSignalHandler;
  sigemptyset(&saInt.sa_mask);
  saInt.sa_flags |= SA_RESTART;

  if (sigaction(SIGINT, &saInt, 0) > 0)
    error(Q("Server: coult not set up SIGINT handler"));

  if (!m_socket.bind(QHostAddress::Any, m_options.port))
    error(Q("Server: coult not listen on port %1").arg(m_options.port));

  debug(Q("Listening on port %1").arg(m_options.port));
}

void
Server::readPendingDatagrams() {
  while (m_socket.hasPendingDatagrams()) {
    QByteArray datagram;
    datagram.resize(m_socket.pendingDatagramSize());

    auto sender     = QHostAddress{};
    auto senderPort = quint16{};

    m_socket.readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);

    processDatagram(datagram);
  }
}

void
Server::processDatagram(QByteArray const &datagram) {
  m_parser.parseDatagram(MemoryStream{datagram});
}

void
Server::handleSigInt() {
  debug(Q("Caught SIGINT; exiting"));

  m_sigIntNotifier->setEnabled(false);
  char tmp;
  ::read(ms_sigIntFD[1], &tmp, sizeof(tmp));

  QCoreApplication::exit(0);

  m_sigIntNotifier->setEnabled(true);
}

void
Server::intSignalHandler(int) {
  char a = 1;
  ::write(ms_sigIntFD[0], &a, sizeof(a));
}
