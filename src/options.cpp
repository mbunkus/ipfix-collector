#include <iostream>

#include "options.h"
#include "storage.h"
#include "util.h"

void
Options::dump()
  const {
  std::cout
    << (Q("Options: port=%1 observationDomainID=%2 ipv4Only=%3 ipv6Only=%4 dumpMode=%5 storageType=%6 storageConfiguration=%7 pcapFile=%8 dumpStart=%9 dumpEnd=%10")
        .arg(port)
        .arg(observationDomainID)
        .arg(ipv4Only)
        .arg(ipv6Only)
        .arg(dumpMode)
        .arg(storage ? storage->metaObject()->className() : "")
        .arg(storageConfiguration)
        .arg(pcapFile)
        .arg(dumpStart.isValid() ? Q("%1 UTC").arg(dumpStart.toString("yyyy-MM-dd HH:mm:ss")) : Q(""))
        .arg(dumpEnd.isValid()   ? Q("%1 UTC").arg(dumpEnd.toString(  "yyyy-MM-dd HH:mm:ss")) : Q(""))
        )
    << std::endl;
}
