#ifndef NULL_STORAGE_H
#define NULL_STORAGE_H

#include <utility>

#include "storage.h"

class NullStorage: public Storage {
  Q_OBJECT
public:
  NullStorage(QObject *parent, Options const &options);
  virtual ~NullStorage();

  virtual bool openWrite();
  virtual void write(FlowData const &data);
};

#endif
