#include "pcap_parser.h"
#include "util.h"

PcapParser::PcapParser(Options const &options)
  : m_options{options}
  , m_parser{options}
  , m_numPacketsProcessed{}
{
}

bool
PcapParser::parse() {
  pcap_t *pcap = nullptr;

  AtScopeExit cleanup([&]() {
    if (pcap)
      pcap_close(pcap);
  });

  char errbuf[PCAP_ERRBUF_SIZE];
  pcap = pcap_open_offline(m_options.pcapFile.toLocal8Bit().data(), errbuf);
  if (!pcap)
    error(Q("Error opening file pcap file %1: %2").arg(m_options.pcapFile).arg(QString::fromLocal8Bit(errbuf)));

  debug(Q("Reading from pcap file %1").arg(m_options.pcapFile));

  auto datalink = pcap_datalink(pcap);
  if (datalink != 1)
    error(Q("Only the Ethernet link type is supported, not %1").arg(datalink));

  pcap_pkthdr header;
  u_char const *data;
  while ((data = pcap_next(pcap, &header)) != nullptr)
    processLayer2Frame(header, data);

  return true;
}

void
PcapParser::processLayer2Frame(pcap_pkthdr const &header,
                               u_char const *data) {
  try {
    auto timestamp = static_cast<uint64_t>(header.ts.tv_sec) * 1000000ull + header.ts.tv_usec;
    doProcessLayer2Frame(MemoryStream{data, std::min(header.caplen, header.len)}, timestamp);

    // ++m_numPacketsProcessed;
    // if (!(m_numPacketsProcessed % 20))
    //   expireFragments(timestamp);

  } catch (MemoryStream::EndOfStreamX const &) {
    debug(Q("X caught end of stream on packet capture length %1 length %2").arg(header.caplen).arg(header.len));
  }
}


void
PcapParser::doProcessLayer2Frame(MemoryStream mem,
                                 uint64_t timestamp) {
  // Ethernet header: 14 bytes
  // IP header: usually 20 bytes (stored in »length« field)
  // UDP header: 8 bytes
  mem.seek(12);
  auto type = mem.getUInt16BE();
  if (type != 0x0800) {
    debug(Q("Only IP frames are supported, not %1").arg(type));
    return;
  }

  unsigned int value    = mem.getUInt8();
  auto ipVersion        = value >> 4;
  auto ipHeaderLength   = (value & 0x0f) << 2;

  mem.seek(16);
  auto ipLength         = mem.getUInt16BE();
  auto ipIdentification = mem.getUInt16BE();
  value                 = mem.getUInt16BE();
  auto ipFlags          = value >> 13;
  auto ipMoreFragments  = !!(ipFlags & 1);
  auto ipFragmentOffset = (value & 0x1fff) << 3;
  auto ipPayloadLength  = ipLength - ipHeaderLength;

  if (ipVersion != 4) {
    debug(Q("Only IPv4 is supported, not %1").arg(ipVersion));
    return;
  }

  mem.seek(23);
  auto layer4Protocol = mem.getUInt8();
  if (layer4Protocol != 17) {
    debug(Q("Only UDP is supported, not %1").arg(layer4Protocol));
    return;
  }

  auto &fragment = m_ipFragments[ipIdentification];

  if (!fragment.timestamp)
    fragment.timestamp = timestamp;

  if (!ipMoreFragments)
    fragment.totalBytes = ipFragmentOffset + ipPayloadLength;

  fragment.availableBytes += ipPayloadLength;

  if (static_cast<std::size_t>(fragment.data.size()) < (ipFragmentOffset + ipPayloadLength))
    fragment.data.resize(ipFragmentOffset + ipPayloadLength);

  mem.seek(14 + ipHeaderLength);
  mem.read(fragment.data.data() + ipFragmentOffset, ipPayloadLength);

  if (!fragment.totalBytes || (fragment.totalBytes > fragment.availableBytes))
    return;

  auto layer4Mem = MemoryStream{fragment.data};
  layer4Mem.seek(2);
  auto dstPort = layer4Mem.getUInt16BE();
  if (dstPort == m_options.port) {
    layer4Mem.seek(8);
    m_parser.parseDatagram(layer4Mem);
  }

  m_ipFragments.remove(ipIdentification);
}

void
PcapParser::expireFragments(uint64_t timestamp) {
  auto keys = m_ipFragments.keys();
  for (auto const &ipIdentification : keys) {
    auto diff = std::abs(timestamp - m_ipFragments[ipIdentification].timestamp);
    if (diff < 1000000)
      continue;

    debug(Q("Expiring ID %1, diff %2").arg(ipIdentification).arg(diff));
    m_ipFragments.remove(ipIdentification);
  }
}
