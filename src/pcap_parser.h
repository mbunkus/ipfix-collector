#ifndef PCAP_PARSER_H
#define PCAP_PARSER_H

#include <QObject>
#include <QHash>

#include <pcap.h>

#include "ipfix_parser.h"
#include "options.h"

class QByteArray;
class QString;

class PcapParser : public QObject {
  Q_OBJECT
private:
  struct IPFragment {
    QByteArray data;
    uint64_t timestamp;
    unsigned int totalBytes, availableBytes;
  };

  Options m_options;
  IpfixParser m_parser;
  QHash<uint16_t, IPFragment> m_ipFragments;
  uint64_t m_numPacketsProcessed;

public:
  PcapParser(Options const &options);

  bool parse();

private:
  void processLayer2Frame(pcap_pkthdr const &header, u_char const *data);
  void doProcessLayer2Frame(MemoryStream mem, uint64_t timestamp);
  void expireFragments(uint64_t timestamp);
};

#endif
