#include <iostream>

#include "stdout_storage.h"
#include "util.h"

StdoutStorage::StdoutStorage(QObject *parent,
                             Options const &options)
  : Storage{parent, options}
{
}

StdoutStorage::~StdoutStorage() {
}

bool
StdoutStorage::openWrite() {
  return true;
}

void
StdoutStorage::write(FlowData const &data) {
  std::cout << data.toString() << std::endl;
}
