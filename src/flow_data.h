#ifndef FLOW_DATA_H
#define FLOW_DATA_H

#include <QList>

class MemoryStream;
class QString;

class IPAddress {
private:
  QList<uint32_t> m_address;

public:
  IPAddress();
  explicit IPAddress(uint32_t ipv4Address);
  explicit IPAddress(QList<uint32_t> const &ipv6Address);

  bool isSet() const;
  QString toString() const;
  void readIPv4From(MemoryStream &mem);
  void readIPv6From(MemoryStream &mem);

  bool isIPv4() const;
  bool isIPv6() const;

public:
  static IPAddress fromString(QString const &string);

private:
  QString ipv4ToString() const;
  QString ipv6ToString() const;
};

class FlowData {
public:
  uint64_t m_timestamp;
  IPAddress m_srcAddress, m_dstAddress;
  unsigned int m_observationDomainID, m_srcPort, m_dstPort, m_protocol;
  uint64_t m_bytes, m_packets;

public:
  FlowData();

  bool isValid() const;
  bool isIPv4() const;
  bool isIPv6() const;
  QString toString() const;
};

#endif
