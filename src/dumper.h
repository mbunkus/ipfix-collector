#ifndef DUMPER_H
#define DUMPER_H

#include "options.h"

class Dumper {
private:
  Options m_options;

public:
  Dumper(Options const &options);

  int process();
};

#endif
