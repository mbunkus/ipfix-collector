#ifndef UTIL_H
#define UTIL_H

#include <cstdint>
#include <functional>
#include <ostream>

class QString;

#define T(s) Q(s)
QString Q(QString const &string);
QString Q(char const *string);

class AtScopeExit {
private:
  std::function<void()> m_code;
public:
  AtScopeExit(const std::function<void()> &code) : m_code(code) {}
  ~AtScopeExit() {
    m_code();
  }
};

void debug(QString const &message);
void error(QString const &message);

uint64_t getUIntBE(void const *buf, size_t numBytes);
uint16_t getUInt16BE(void const *buf);
uint32_t getUInt24BE(void const *buf);
uint32_t getUInt32BE(void const *buf);
uint64_t getUInt64BE(void const *buf);

inline std::ostream &
operator <<(std::ostream &out,
            QString const &string) {
  out << string.toLocal8Bit().data();

  return out;
}

inline std::wostream &
operator <<(std::wostream &out,
            QString const &string) {
  out << string.toStdWString();
  return out;
}

#endif
