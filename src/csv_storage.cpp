#include <memory>
#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QFile>
#include <QRegularExpression>

#include "csv_storage.h"
#include "flow_data.h"
#include "util.h"

CsvStorage::CsvStorage(QObject *parent,
                       Options const &options)
  : Storage{parent, options}
  , m_maxDurationPerFile{5 * 60 * 1000}
  , m_currentTimestamp{}
  , m_firstTimestampInFile{}
  , m_searchedForFiles{}
{
  configure(options.storageConfiguration);
  connect(QCoreApplication::instance(), SIGNAL(aboutToQuit()), this, SLOT(aboutToQuit()));
}

CsvStorage::~CsvStorage() {
}

bool
CsvStorage::openRead() {
  if (m_file) {
    m_file->close();
    m_file.reset();
  }

  if (!m_searchedForFiles) {
    searchForFiles();
    filterFiles();
  }

  while (!m_filesToRead.isEmpty()) {
    m_file = std::make_shared<QFile>(m_filesToRead.takeFirst());
    if (m_file->open(QIODevice::ReadOnly) && readColumns())
      return true;

    m_file.reset();
  }

  return false;
}

std::pair<FlowData, bool>
CsvStorage::read() {
  while (true) {
    if (!m_file && !openRead())
      return std::make_pair(FlowData{}, false);

    auto data = readDataLine();
    if (!data.isValid())
      continue;

    if (m_options.dumpStart.isValid() && (data.m_timestamp < static_cast<uint64_t>(m_options.dumpStart.toMSecsSinceEpoch())))
      continue;

    if (m_options.dumpEnd.isValid() && (data.m_timestamp >= static_cast<uint64_t>(m_options.dumpEnd.toMSecsSinceEpoch()))) {
      m_file.reset();
      continue;
    }

    return std::make_pair(data, true);
  }
}

bool
CsvStorage::openWrite() {
  auto timestampDT = QDateTime::fromMSecsSinceEpoch(m_currentTimestamp).toUTC();
  auto fileName    = Q("%1%2.csv").arg(m_baseDirPath, timestampDT.toString("yyyy/MM/dd/HHmmss"));

  auto dir = QFileInfo{fileName}.dir();
  if (!dir.exists() && !dir.mkpath(Q(".")))
    error(Q("CsvStorage: could not create the directory %1").arg(dir.path()));

  m_file = std::make_shared<QFile>(fileName);
  if (!m_file->open(QIODevice::WriteOnly))
    error(Q("CsvStorage: could not open %1 for writing").arg(fileName));

  m_file->write(Q("timestamp;observationDomainID;protocol;srcAddress;srcPort;dstAddress;dstPort;bytes;packets\n").toUtf8());
  m_firstTimestampInFile = m_currentTimestamp;

  return true;
}

void
CsvStorage::write(FlowData const &data) {
  if (!m_file || (std::abs(data.m_timestamp - m_firstTimestampInFile) > m_maxDurationPerFile)) {
    m_currentTimestamp = data.m_timestamp;
    openWrite();
  }

  m_file->write(Q("%1;%2;%3;%4;%5;%6;%7;%8;%9\n")
                .arg(data.m_timestamp)
                .arg(data.m_observationDomainID)
                .arg(data.m_protocol)
                .arg(data.m_srcAddress.toString())
                .arg(data.m_srcPort)
                .arg(data.m_dstAddress.toString())
                .arg(data.m_dstPort)
                .arg(data.m_bytes)
                .arg(data.m_packets)
                .toUtf8());

  auto now = QDateTime::currentMSecsSinceEpoch();
  if ((now - m_lastFlush) > (20 * 1000)) {
    m_lastFlush = now;
    m_file->flush();
  }
}

void
CsvStorage::configure(QString const &string) {
  auto parts    = string.split(Q(";"));
  m_baseDirPath = parts[0];
  if (m_baseDirPath.isEmpty())
    error(Q("CsvStorage: no directory base name provided"));

  if (!m_baseDirPath.endsWith(Q("/")))
    m_baseDirPath += Q("/");

  parts.pop_front();
  for (auto const &part : parts) {
    auto kv = part.split(Q("="));
    if (kv[0] == Q("maxDuration"))
      m_maxDurationPerFile = kv[1].toULongLong() * 1000;

    else
      error(Q("CsvStorage: Unknown option %1").arg(kv[0]));
  }
}

void
CsvStorage::aboutToQuit() {
  debug(Q("About to quit"));
  if (m_file) {
    m_file->close();
    m_file.reset();
  }
}

void
CsvStorage::searchForFiles() {
  m_searchedForFiles  = true;
  auto directoryPaths = QStringList{} << QDir{m_baseDirPath}.absolutePath();

  while (!directoryPaths.isEmpty()) {
    auto directoryPath     = directoryPaths.takeFirst();
    auto directoryObj      = QDir{directoryPath};

    auto subDirectoryPaths = directoryObj.entryList(QStringList{},               QDir::AllDirs | QDir::NoDotAndDotDot);
    auto filePaths         = directoryObj.entryList(QStringList{} << Q("*.csv"), QDir::Files);

    for (auto const &subDirectoryPath : subDirectoryPaths)
      directoryPaths += directoryObj.filePath(subDirectoryPath);

    for (auto const &filePath : filePaths)
      m_filesToRead += directoryObj.filePath(filePath);
  }

  m_filesToRead.sort();
}

void
CsvStorage::filterFiles() {
  if (!m_options.dumpStart.isValid() && !m_options.dumpEnd.isValid())
    return;

  auto filteredFiles    = QStringList{};
  auto re               = QRegularExpression{Q("(\\d{4})/(\\d{2})/(\\d{2})/(\\d{2})(\\d{2})(\\d{2})\\.csv$")};
  auto previousFileName = Q("");

  for (auto const &fileName : m_filesToRead) {
    auto match = re.match(fileName);
    if (!match.hasMatch())
      continue;

    auto dt = QDateTime{ QDate{match.captured(1).toInt(), match.captured(2).toInt(), match.captured(3).toInt()},
                         QTime{match.captured(4).toInt(), match.captured(5).toInt(), match.captured(6).toInt()},
                         Qt::UTC };

    if (m_options.dumpEnd.isValid() && (dt >= m_options.dumpEnd))
      break;

    if (!m_options.dumpStart.isValid())
      filteredFiles << fileName;

    else {
      if (dt >= m_options.dumpStart)
        filteredFiles << previousFileName;

      previousFileName = fileName;
    }
  }

  if (!previousFileName.isEmpty() && !filteredFiles.isEmpty())
    filteredFiles << previousFileName;

  m_filesToRead = filteredFiles;
}

QString
CsvStorage::readLine() {
  auto line = QString::fromUtf8(m_file->readLine());
  return line.replace(QRegularExpression{"[\\r\\n]+$"}, "");
}

bool
CsvStorage::readColumns() {
  m_file->seek(0);

  auto line = readLine();
  if (line.isEmpty())
    return false;

  auto columns = line.split(Q(";"));
  if (columns.size() < 2)
    return false;

  m_columnMap.clear();
  for (auto idx = 0, end = columns.size(); idx < end; ++idx)
    m_columnMap[columns[idx]] = idx;

  return true;
}

FlowData
CsvStorage::readDataLine() {
  auto data = FlowData{};
  auto line = readLine();
  if (line.isEmpty())
    return data;

  auto fields = line.split(Q(";"));

  if (m_columnMap.contains("timestamp"))
    data.m_timestamp = fields[m_columnMap["timestamp"]].toULongLong();

  if (m_columnMap.contains("observationDomainID"))
    data.m_observationDomainID = fields[m_columnMap["observationDomainID"]].toUInt();

  if (m_columnMap.contains("protocol"))
    data.m_protocol = fields[m_columnMap["protocol"]].toUInt();

  if (m_columnMap.contains("srcAddress"))
    data.m_srcAddress = IPAddress::fromString(fields[m_columnMap["srcAddress"]]);

  if (m_columnMap.contains("srcPort"))
    data.m_srcPort = fields[m_columnMap["srcPort"]].toUInt();

  if (m_columnMap.contains("dstAddress")) {
    data.m_dstAddress = IPAddress::fromString(fields[m_columnMap["dstAddress"]]);
    debug(Q("--> got %1").arg(data.m_dstAddress.toString()));
  }

  if (m_columnMap.contains("dstPort"))
    data.m_dstPort = fields[m_columnMap["dstPort"]].toUInt();

  if (m_columnMap.contains("bytes"))
    data.m_bytes = fields[m_columnMap["bytes"]].toUInt();

  if (m_columnMap.contains("packets"))
    data.m_packets = fields[m_columnMap["packets"]].toUInt();

  debug(Q("val? %1 for %2").arg(data.isValid()).arg(data.toString()));

  return data;
}
