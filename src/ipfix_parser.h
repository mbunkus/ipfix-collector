#ifndef IPFIX_PARSER_H
#define IPFIX_PARSER_H

#include <QObject>
#include <QHash>

#include "flow_data.h"
#include "memory_stream.h"
#include "options.h"

class QByteArray;
class QString;

class IpfixParser : public QObject {
  Q_OBJECT
private:
  enum TemplateID {
    TemplateSetID    = 0x0002,
    MinimumDataSetID = 0x0100,
  };

  enum TemplateType {
    TypeBytes            = 0x0001,
    TypePackets          = 0x0002,
    TypeNumFlows         = 0x0003,
    TypeProtocol         = 0x0004,
    TypeLayer4SrcPort    = 0x0007,
    TypeIPv4SrcAddr      = 0x0008,
    TypeInputInterface   = 0x000a,
    TypeLayer4DstPort    = 0x000b,
    TypeIPv4DstAddr      = 0x000c,
    TypeOutputInterface  = 0x000e,
    TypeSrcAddr          = 0x001b,
    TypeDstAddr          = 0x001c,
    TypeDirection        = 0x003d,
    TypeTotalBytes       = 0x0055,
    TypeTotalPackets     = 0x0056,
    TypeFlowStartSeconds = 0x0096,
    TypeFlowEndSeconds   = 0x0097,
  };

  struct TemplateField {
    TemplateType type;
    unsigned int length, pen;
  };

  struct Template {
    QList<TemplateField> fields;
    unsigned int expectedFlowSize;

    Template();
    void reset(unsigned int reserveFieldStorage);
    void addField(TemplateField const &field);
  };

  Options m_options;
  QHash<uint16_t, Template> m_templates;
  QList<FlowData> m_dataToAccount;

  uint32_t m_observationDomainID, m_flowSequence, m_expectedNextFlowSequence;
  uint64_t m_datagramTimestamp;

public:
  IpfixParser(Options const &options);

  void parseDatagram(MemoryStream datagram);

private:
  void doParseDatagram(MemoryStream datagram);
  size_t parseSet(MemoryStream mem);
  void parseDataSet(uint16_t setID, MemoryStream mem);
  void parseTemplateSet(MemoryStream mem);
  size_t parseTemplate(MemoryStream mem);
  void parseField(FlowData &data, MemoryStream &mem, TemplateField const &fieldTemplate);
};

#endif
