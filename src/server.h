#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QUdpSocket>

#include "ipfix_parser.h"
#include "options.h"

class QByteArray;
class QSocketNotifier;

class Server : public QObject {
  Q_OBJECT
private:
  Options m_options;
  QUdpSocket m_socket;
  IpfixParser m_parser;
  QSocketNotifier *m_sigIntNotifier;

private:
  static int ms_sigIntFD[2];

public:
  Server(QObject *parent, Options const &options);
  void initialize();

private slots:
  void readPendingDatagrams();
  void handleSigInt();

private:
  void processDatagram(QByteArray const &datagram);

public:
  static void intSignalHandler(int);
};

#endif
