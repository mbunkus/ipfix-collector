#include <QDateTime>
#include <QRegularExpression>
#include <QStringList>

#include "flow_data.h"
#include "memory_stream.h"
#include "util.h"

IPAddress::IPAddress() {
}

IPAddress::IPAddress(uint32_t ipv4Address) {
  m_address << ipv4Address;
}

IPAddress::IPAddress(QList<uint32_t> const &ipv6Address) {
  m_address = ipv6Address;
}

bool
IPAddress::isSet()
  const {
  return isIPv4() || isIPv6();
}

bool
IPAddress::isIPv4()
  const {
  return m_address.size() == 1;
}

bool
IPAddress::isIPv6()
  const {
  return m_address.size() == 8;
}

QString
IPAddress::toString()
  const {
  return m_address.size() == 1 ? ipv4ToString()
       : m_address.size() == 8 ? ipv6ToString()
       :                         Q("");
}

QString
IPAddress::ipv4ToString()
  const {
  return Q("%1.%2.%3.%4")
    .arg((m_address[0] & 0xff000000) >> 24)
    .arg((m_address[0] & 0x00ff0000) >> 16)
    .arg((m_address[0] & 0x0000ff00) >>  8)
    .arg((m_address[0] & 0x000000ff));
}

// void
// IPAddress::_runTests() {
//   auto tests = QList< QList<uint32_t> >{};
//   tests << (QList<uint32_t>{} << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000);
//   tests << (QList<uint32_t>{} << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0001);
//   tests << (QList<uint32_t>{} << 0x0001 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000);
//   tests << (QList<uint32_t>{} << 0x0001 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0000 << 0x0001);
//   tests << (QList<uint32_t>{} << 0x2001 << 0x06f8 << 0x13dc << 0x0002 << 0x0000 << 0x0000 << 0xfeed << 0x0001);
//   tests << (QList<uint32_t>{} << 0x2001 << 0x06f8 << 0x13dc << 0x0002 << 0xfeed << 0x0000 << 0x0000 << 0x0001);
//   tests << (QList<uint32_t>{} << 0x2001 << 0x06f8 << 0x0000 << 0x0002 << 0xfeed << 0x0000 << 0x0000 << 0x0001);
//   tests << (QList<uint32_t>{} << 0x2001 << 0x0000 << 0x0000 << 0x0002 << 0xfeed << 0x0000 << 0x0000 << 0x0001);
//   tests << (QList<uint32_t>{} << 0x2001 << 0x0000 << 0x0000 << 0x0002 << 0x0000 << 0x0000 << 0x0000 << 0x0001);

//   for (auto const &test : tests) {
//     QString out;
//     for (auto const &num : test)
//       out += Q("%1 ").arg(num, 4, 16, QLatin1Char('0'));
//     debug(Q("%1=> %2").arg(out, IPAddress{test}.toString()));
//   }
//   ::exit(42);
// }

QString
IPAddress::ipv6ToString()
  const {
  static QRegularExpression s_re{"^(?:0:)+(?:0$)?|(?::0)+$|:(?:0:)+"};

  QStringList parts;
  for (auto const &value : m_address)
    parts << Q("%1").arg(value, 0, 16);

  auto fullAddress = parts.join(Q(":"));
  auto itr         = s_re.globalMatch(fullAddress);
  auto position    = -1;
  auto length      = 0;

  while (itr.hasNext()) {
    auto match         = itr.next();
    auto currentLength = match.capturedLength(0);

    if (currentLength > length) {
      position = match.capturedStart(0);
      length   = currentLength;
    }
  }

  if (position != -1)
    fullAddress.replace(position, length, Q("::"));

  return fullAddress;
}

void
IPAddress::readIPv4From(MemoryStream &mem) {
  m_address.clear();
  m_address.reserve(1);

  m_address << mem.getUInt32BE();
}

void
IPAddress::readIPv6From(MemoryStream &mem) {
  m_address.clear();
  m_address.reserve(8);

  for (auto idx = 0; idx < 8; ++idx)
    m_address << mem.getUInt16BE();
}

IPAddress
IPAddress::fromString(QString const &string) {
  static QRegularExpression s_reIPv4{"^(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)$"};
  static QRegularExpression s_reIPv6{"^[\\da-fA-F:]+$"};

  auto match = s_reIPv4.match(string);
  if (match.hasMatch()) {
    auto address = 0u;
    for (auto idx = 1; idx <= 4; ++idx)
      address = (address << 8) | match.captured(idx).toUInt();
    return IPAddress{address};
  }

  match = s_reIPv6.match(string);
  if (!match.hasMatch())
    return IPAddress{};

  auto strParts = string.split("::");
  if (strParts.size() > 2)
    return IPAddress{};

  auto wordsLists = QList< QList<uint32_t> >{} << QList<uint32_t>{} << QList<uint32_t>{};

  for (auto idx = 0, end = strParts.size(); idx < end; ++idx) {
    if (strParts[idx].isEmpty())
      continue;

    auto strWords = strParts[idx].split(":");
    for (auto const &strWord : strWords)
      wordsLists[idx] << strWord.toUInt(nullptr, 16);
  }

  for (auto idx = wordsLists[0].size() + wordsLists[1].size(); idx < 8; ++idx)
    wordsLists[0] << 0;

  wordsLists[0] += wordsLists[1];

  return wordsLists[0].size() == 8 ? IPAddress{wordsLists[0]} : IPAddress{};
}

// ----------------------------------------------------------------------

FlowData::FlowData()
  : m_timestamp{}
  , m_srcAddress{}
  , m_dstAddress{}
  , m_observationDomainID{}
  , m_srcPort{}
  , m_dstPort{}
  , m_protocol{}
  , m_bytes{}
  , m_packets{}
{
}

bool
FlowData::isValid()
  const {
  return m_srcAddress.isSet() && m_dstAddress.isSet()
    && (m_protocol != 0) && (m_bytes != 0) && (m_packets != 0);
}

bool
FlowData::isIPv4()
  const {
  return !isIPv6();
}

bool
FlowData::isIPv6()
  const {
  return m_srcAddress.isIPv6() || m_dstAddress.isIPv6();
}

QString
FlowData::toString()
  const {
  if (!isValid())
    return Q("");

  return Q("%8 obsDomID %9 proto %1 from %2 port %3 to %4 port %5 bytes %6 packets %7")
    .arg(m_protocol).arg(m_srcAddress.toString()).arg(m_srcPort).arg(m_dstAddress.toString()).arg(m_dstPort).arg(m_bytes).arg(m_packets)
    .arg(QDateTime::fromMSecsSinceEpoch(m_timestamp).toString("yyyy-MM-dd HH:mm:ss.zzz")).arg(m_observationDomainID);
}
