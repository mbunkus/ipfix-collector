#ifndef CSV_STORAGE_H
#define CSV_STORAGE_H

#include <memory>
#include <utility>
#include <QHash>
#include <QString>
#include <QStringList>

#include "storage.h"

class QFile;

class CsvStorage: public Storage {
  Q_OBJECT
protected:
  uint64_t m_lastTimestampOut;
  QString m_baseDirPath;
  uint64_t m_maxDurationPerFile, m_currentTimestamp, m_firstTimestampInFile, m_lastFlush;
  std::shared_ptr<QFile> m_file;

  bool m_searchedForFiles;
  QStringList m_filesToRead;
  QHash<QString, unsigned int> m_columnMap;

public:
  CsvStorage(QObject *parent, Options const &options);
  virtual ~CsvStorage();

  virtual bool openRead();
  virtual bool openWrite();

  virtual void write(FlowData const &data);
  virtual std::pair<FlowData, bool> read();

protected:
  void configure(QString const &string);
  void searchForFiles();
  void filterFiles();
  bool readColumns();
  FlowData readDataLine();
  QString readLine();

private slots:
  void aboutToQuit();
};

#endif
