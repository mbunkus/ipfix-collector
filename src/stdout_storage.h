#ifndef STDOUT_STORAGE_H
#define STDOUT_STORAGE_H

#include <utility>

#include "storage.h"

class StdoutStorage: public Storage {
  Q_OBJECT
public:
  StdoutStorage(QObject *parent, Options const &options);
  virtual ~StdoutStorage();

  virtual bool openWrite();
  virtual void write(FlowData const &data);
};

#endif
