#include <QCoreApplication>
#include <QtCore>

#include <iostream>

#include "csv_storage.h"
#include "dumper.h"
#include "null_storage.h"
#include "options.h"
#include "pcap_parser.h"
#include "server.h"
#include "stdout_storage.h"
#include "util.h"

static void
initialize() {
  qsrand(QTime{0,0,0}.secsTo(QTime::currentTime()));

  QCoreApplication::setApplicationName("ipfix-collector");
  QCoreApplication::setApplicationVersion("1.0");
}

static void
parseStorageArg(Options &options,
                QString const &arg) {
  auto match = QRegularExpression{Q("^(csv|null|display)(?::(.*))?")}.match(arg);
  if (!match.hasMatch())
    error(Q("Invalid storage configuration string; general syntax: type:options"));

  auto storageType             = match.captured(1);
  options.storageConfiguration = match.captured(2);

  if (storageType == Q("csv"))
    options.storage = new CsvStorage{QCoreApplication::instance(), options};

  else if (storageType == Q("null"))
    options.storage = new NullStorage{QCoreApplication::instance(), options};

  else if (storageType == Q("display"))
    options.storage = new StdoutStorage{QCoreApplication::instance(), options};

  else
    error(Q("Unknown storage type %1").arg(storageType));
}

static QDateTime
parseDateArg(QString const &arg) {
  auto match = QRegularExpression{Q("^(\\d+)$")}.match(arg);
  if (match.hasMatch())
    return QDateTime::fromMSecsSinceEpoch(arg.toULongLong() * 1000).toUTC();

  match = QRegularExpression{Q("^(\\d{4})-(\\d{2})-(\\d{2})(?: (\\d{2}):(\\d{2})(?::(\\d{2}))?)?$")}.match(arg);
  if (!match.hasMatch())
    error(Q("Invalid date format; general syntax: 'YYYY-mm-dd HH:MM:SS' or simply 'YYYY-mm-dd' or seconds since the epoch"));

  auto hh = match.capturedLength(4) > 0 ? match.captured(4).toInt() : 0;
  auto mm = match.capturedLength(5) > 0 ? match.captured(5).toInt() : 0;
  auto ss = match.capturedLength(6) > 0 ? match.captured(6).toInt() : 0;
  auto dt = QDateTime{QDate{match.captured(1).toInt(), match.captured(2).toInt(), match.captured(3).toInt()}};
  dt.setTime(QTime{hh, mm, ss});

  return dt.toUTC();
}

static Options
parseCommandLine() {
  QCommandLineParser parser;
  parser.setApplicationDescription("IPFIX data collector");

  QCommandLineOption ipv4Only(QStringList{} << "4" << "ipv4-only", T("Account IPv4 traffic only (default: both IPv4 and IPv6)."));
  parser.addOption(ipv4Only);

  QCommandLineOption ipv6Only(QStringList{} << "6" << "ipv6-only", T("Account IPv6 traffic only (default: both IPv4 and IPv6)."));
  parser.addOption(ipv6Only);

  QCommandLineOption dumpMode(QStringList{} << "d" << "dump-mode", T("Enable dump mode (read from storage and output to stdout)."));
  parser.addOption(dumpMode);

  QCommandLineOption observationDomainID(QStringList{} << "i" << "observation-domain-id", T("Handle observation domain with ID <id> (default: -1 == handle all)."), T("ID"), "-1");
  parser.addOption(observationDomainID);

  QCommandLineOption port(QStringList{} << "p" << "port", T("Listen on port number <port> (default: 4739)."), T("port number"), "4739");
  parser.addOption(port);

  QCommandLineOption readFile(QStringList{} << "r" << "read-file", T("Read IPFIX packets from the PCAP file <file name> instead of listening on a UDP socket."), T("file name"));
  parser.addOption(readFile);

  QCommandLineOption storage(QStringList{} << "s" << "storage", T("Use specified storage driver and configuration (default: 'display' without configuration)."), T("driver:configuration"), "display");
  parser.addOption(storage);

  QCommandLineOption start("start", T("Only include data starting at <timestamp>."), T("timestamp"));
  parser.addOption(start);

  QCommandLineOption end("end", T("Only include data up to and excluding <timestamp>."), T("timestamp"));
  parser.addOption(end);

  QCommandLineOption help(QStringList{} << "h" << "help", T("Displays this help."));
  parser.addOption(help);

  auto const version = parser.addVersionOption();

  parser.process(*QCoreApplication::instance());

  if (parser.isSet(version)) {
    std::cout << QCoreApplication::applicationName() << " " << QCoreApplication::applicationVersion() << std::endl;
    ::exit(0);
  }

  if (parser.isSet(help)) {
    std::cout << parser.helpText()
              << std::endl
              << T("Operating modes:\n")
              << T("  * Server mode: In this mode IPFIX packets are read from a UDP socket given\n"
                   "    with --port. Accounted data is written to the configured storage.\n")
              << T("  * Parser mode: In this mode IPFIX packets are read from a PCAP dump file\n"
                   "    given with --read-file (that option enables this mode). Accounted data is\n"
                   "    written to the configured storage.\n")
              << T("  * Dump mode: In this mode accounted data is read from the configured storage\n"
                   "    and output to the standard output.\n")
              << std::endl
              << T("Supported storages:\n")
              << T("  * null – No configuration. Does not write anything at all. Cannot read\n"
                   "    packages in dump mode.\n")
              << T("  * display – No configuration. Writes one line per accounted flow to standard\n"
                   "    output. Cannot read packages in dump mode.\n")
              << T("  * csv – Writes CSV files. Configuration consists of the base directory name\n"
                   "    in which sub-directories per year, month and day are created. CSV files\n"
                   "    are rotated every 300 seconds. This storage can read data from such an\n"
                   "    existing directory/file hierarchy in dump mode.\n")
              << std::endl
              << T("Timestamp formats:\n")
              << T("  Timestamps can be given in two generally different formats:\n")
              << T("  * A number of seconds since the start of the epoch\n")
              << T("  * The ISO 8601 format with either day (YYYY-mm-dd) or second\n"
                   "    precision (YYYY-mm-dd HH:MM:SS)")
              << std::endl
              << T("  All timestamps are considered to be given in the system's local time zone.\n");

    ::exit(0);
  }

  if (!parser.positionalArguments().isEmpty() || !parser.unknownOptionNames().isEmpty())
    error(T("Unhandled/unknown command line arguments given."));

  Options options;
  options.ipv4Only            = parser.isSet(ipv4Only);
  options.ipv6Only            = parser.isSet(ipv6Only);
  options.dumpMode            = parser.isSet(dumpMode);
  options.port                = parser.value(port).toUInt();
  options.observationDomainID = parser.value(observationDomainID).toInt();
  options.pcapFile            = parser.value(readFile);

  parseStorageArg(options, parser.value(storage));

  if (parser.isSet(start))
    options.dumpStart = parseDateArg(parser.value(start));

  if (parser.isSet(end))
    options.dumpEnd = parseDateArg(parser.value(end));

  return options;
}

int
main(int argc,
     char *argv[]) {
  QCoreApplication app{argc, argv};

  initialize();

  auto options = parseCommandLine();

  if (options.dumpMode) {
    if (!options.storage)
      error(Q("No storage to read from configured"));

    return Dumper{options}.process();
  }

  if (options.pcapFile.isEmpty()) {
    auto server = new Server{&app, options};
    server->initialize();
    return app.exec();
  }

  PcapParser parser{options};
  return parser.parse() ? 0 : 1;
}
