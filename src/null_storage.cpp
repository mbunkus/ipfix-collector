#include "null_storage.h"
#include "util.h"

NullStorage::NullStorage(QObject *parent,
                         Options const &options)
  : Storage{parent, options}
{
}

NullStorage::~NullStorage() {
}

bool
NullStorage::openWrite() {
  return true;
}

void
NullStorage::write(FlowData const &) {
}
