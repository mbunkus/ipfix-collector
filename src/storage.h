#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <utility>

#include "flow_data.h"
#include "options.h"

class Storage: public QObject {
  Q_OBJECT
protected:
  Options m_options;

public:
  Storage(QObject *parent, Options const &options);
  virtual ~Storage();

  virtual bool openRead();
  virtual bool openWrite() = 0;

  virtual std::pair<FlowData, bool> read();
  virtual void write(FlowData const &data) = 0;
};

#endif
