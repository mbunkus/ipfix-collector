#include <cstring>
#include <QByteArray>
#include <QTextStream>

#include "memory_stream.h"
#include "util.h"

MemoryStream::EndOfStreamX::EndOfStreamX(size_t missingBytes)
  : m_missingBytes{missingBytes}
{
}

// ----------------------------------------------------------------------

MemoryStream::MemoryStream(void const *ptr,
                           std::size_t size) {
  init(ptr, size);
}

MemoryStream::MemoryStream(QByteArray const &data) {
  init(data.data(), data.size());
}

MemoryStream::MemoryStream(MemoryStream const &mem) {
  init(mem.m_ptr, mem.getRemainingBytes());
}

void
MemoryStream::init(void const *ptr,
                   std::size_t size) {
  m_start = reinterpret_cast<unsigned char const *>(ptr);
  m_end   = m_start + size;
  m_ptr   = m_start;
}

std::size_t
MemoryStream::getPosition()
  const {
  return m_ptr - m_start;
}

std::size_t
MemoryStream::getSize()
  const {
  return m_end - m_start;
}

std::size_t
MemoryStream::getRemainingBytes()
  const {
  return m_end > m_ptr ? m_end - m_ptr : 0;
}

bool
MemoryStream::isEmpty()
  const {
  return m_end <= m_ptr;
}

uint8_t
MemoryStream::getUInt8() {
  assertBytesLeft(1);

  auto value = *m_ptr;
  m_ptr     += 1;

  return value;
}

uint64_t
MemoryStream::getUIntBE(std::size_t numBytes) {
  assertBytesLeft(numBytes);

  auto value = ::getUIntBE(m_ptr, numBytes);
  m_ptr     += numBytes;

  return value;
}

uint16_t
MemoryStream::getUInt16BE() {
  return getUIntBE(2);
}

uint32_t
MemoryStream::getUInt24BE() {
  return getUIntBE(3);
}

uint32_t
MemoryStream::getUInt32BE() {
  return getUIntBE(4);
}

uint64_t
MemoryStream::getUInt64BE() {
  return getUIntBE(8);
}

void
MemoryStream::skip(std::size_t count) {
  assertBytesLeft(count);
  m_ptr += count;
}

void
MemoryStream::seek(std::size_t offset) {
  if (offset > getSize())
    throw EndOfStreamX{getSize() - offset};
  m_ptr = m_start + offset;
}

MemoryStream
MemoryStream::slice(std::size_t count) {
  assertBytesLeft(count);
  return MemoryStream{m_ptr, count};
}

void
MemoryStream::read(void *dst,
                   std::size_t count) {
  assertBytesLeft(count);
  std::memcpy(dst, m_ptr, count);
  m_ptr += count;
}

void
MemoryStream::assertBytesLeft(std::size_t count)
  const {
  if (!m_start || ((m_ptr + count) > m_end))
    throw EndOfStreamX{!m_start ? count : count - (m_end - m_ptr)};
}

QString
MemoryStream::hexDump(std::size_t count,
                      std::size_t startOffset) {
  QString dumpString, asciiString;
  QTextStream dump{&dumpString}, ascii{&asciiString};

  auto buffer    = startOffset == std::numeric_limits<std::size_t>::max() ? m_ptr : m_start + startOffset;
  auto bufferIdx = 0u;
  auto length    = std::min<std::size_t>(count == 0 ? std::numeric_limits<std::size_t>::max() : count, m_end - buffer);

  while (bufferIdx < length) {
    if ((bufferIdx % 16) == 0) {
      if (0 < bufferIdx) {
        dump << ' ' << asciiString << '\n';
        asciiString.clear();
      }
      dump << QString("%1  ").arg(bufferIdx, 8, 16, QLatin1Char('0'));

    } else if ((bufferIdx % 8) == 0) {
      dump  << ' ';
      ascii << ' ';
    }

    ascii << (((32 <= buffer[bufferIdx]) && (127 > buffer[bufferIdx])) ? static_cast<char>(buffer[bufferIdx]) : '.');
    dump  << QString("%1 ").arg(static_cast<unsigned int>(buffer[bufferIdx]), 2, 16, QLatin1Char('0'));

    ++bufferIdx;
  }

  if ((bufferIdx % 16) != 0)
    dump << QString(3u * (16 - (bufferIdx % 16)) + ((bufferIdx % 8) ? 1 : 0), QLatin1Char(' '));
  dump << ' ' << asciiString << '\n';

  return dumpString;
}
