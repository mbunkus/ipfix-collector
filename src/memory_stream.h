#ifndef MEMORY_STREAM_H
#define MEMORY_STREAM_H

#include <cstdint>
#include <limits>

class QByteArray;
class QString;

class MemoryStream {
public:
  class EndOfStreamX {
  public:
    size_t m_missingBytes;
    EndOfStreamX(size_t missingBytes);
  };

private:
  unsigned char const *m_start, *m_end, *m_ptr;

public:
  MemoryStream(void const *ptr, std::size_t size);
  MemoryStream(QByteArray const &data);
  MemoryStream(MemoryStream const &mem);

  std::size_t getPosition() const;
  std::size_t getSize() const;
  std::size_t getRemainingBytes() const;
  bool isEmpty() const;

  uint8_t getUInt8();
  uint64_t getUIntBE(std::size_t numBytes);
  uint16_t getUInt16BE();
  uint32_t getUInt24BE();
  uint32_t getUInt32BE();
  uint64_t getUInt64BE();

  void read(void *dst, std::size_t count);

  void skip(std::size_t count);
  void seek(std::size_t position);
  MemoryStream slice(std::size_t count);

  QString hexDump(std::size_t count = std::numeric_limits<std::size_t>::max(), std::size_t startOffset = std::numeric_limits<std::size_t>::max());

private:
  void init(void const *ptr, std::size_t size);
  void assertBytesLeft(std::size_t count) const;
};

#endif
