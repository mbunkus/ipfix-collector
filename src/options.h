#ifndef OPTIONS_H
#define OPTIONS_H

#include <QDateTime>
#include <QString>

class Storage;

struct Options {
  unsigned int port;
  int observationDomainID;
  bool ipv4Only, ipv6Only, dumpMode;
  Storage *storage;
  QString storageConfiguration, pcapFile;
  QDateTime dumpStart, dumpEnd;

  Options()
    : port{4739}
    , observationDomainID{-1}
    , ipv4Only{}
    , ipv6Only{}
    , dumpMode{}
    , storage{}
  {
  }

  void dump() const;
};

#endif
