#include <QDateTime>
#include <QDebug>
#include <QString>

#include <iostream>

#include "util.h"

void
debug(QString const &message) {
  std::cout << QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss ") << message << std::endl;
}

void
error(QString const &message) {
  std::cout << message.toLocal8Bit().data() << std::endl;
  exit(1);
}

uint64_t
getUIntBE(void const *buf,
          size_t numBytes) {
  numBytes     = std::min<size_t>(std::max<size_t>(numBytes, 1), 8);
  auto tmp     = static_cast<unsigned char const *>(buf);
  uint64_t ret = 0;

  for (auto i = 0u; numBytes > i; ++i)
    ret = (ret << 8) + (tmp[i] & 0xff);

  return ret;
}

uint16_t
getUInt16BE(void const *buf) {
  return getUIntBE(buf, 2);
}

uint32_t
getUInt24BE(void const *buf) {
  return getUIntBE(buf, 3);
}

uint32_t
getUInt32BE(void const *buf) {
  return getUIntBE(buf, 4);
}

uint64_t
getUInt64BE(void const *buf) {
  return getUIntBE(buf, 8);
}

QString
Q(char const *string) {
  return QString::fromUtf8(string);
}

QString
Q(QString const &string) {
  return string;
}
