#include "dumper.h"
#include "storage.h"
#include "util.h"

Dumper::Dumper(Options const &options)
  : m_options{options}
{
}

int
Dumper::process() {
  while (true) {
    auto pair = m_options.storage->read();
    if (!pair.second)
      break;

    debug(pair.first.toString());
  }

  return 0;
}
