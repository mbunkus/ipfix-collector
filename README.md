ipfix-collector v(no-release-yet)
=================================

# Introduction

This is a tool for collecting traffic/flow data transmitted as IPFIX
(Internet Protocol Flow Information Export).

The reason it exists is due to several bugs in the and the general
lack of available Open Source IPFIX collector implementations in the
middle of 2014. Some of those bugs have been fixed since, but the
number of packages able to parse and store IPFIX flow data is still
miniscule.

What are the features this tool will support in the future?

1. Run in storage mode: receive IPFIX packets from probes, parse the
   content and store the flow data in one of several backends
   (e.g. CSV files, databases).

2. Run in dump mode: read stored data from databases and dump them
   back in machine-parseable formats like CSV for futher processing in
   other tools.

There's currently no home page apart from this:

https://gitlab.com/mbunkus/ipfix-collector/

Moritz Bunkus <moritz@bunkus.org>

# License

This tool is licensed under the terms of the MIT license (see
`LICENSE.md`) – which basically means that you can do whatever you
want to with it.

# Helping out

Want to help out? Great! This tool needs everything: code
contributions, documentation, fan art ;) Seriously: I'd be happy to
have contributions.

# Requirements

What you need in order to compile this fine program:

- Qt 5 including the qmake tool
- A C++ compiler with support for C++11
- make

# Compilation

Compilation is easy. First create a `Makefile` from the Qt project
file, then run `make`:

    qmake ipfix-collector.pro
    make

If everything succeeds then the resulting binary will be found in
`build/ipfix-collector`.

# Documentation

There's currently no real documentation available apart from the
output of `ipfix-collector --help`.

# Reporting bugs

Keep in mind that this is not release-quality software yet.

I use [GitHub's issue system](https://github.com/mbunkus/ipfix-collector/issues)
as my bug database. You can submit your bug reports there. Please be as
verbose as possible – e.g. include the command line.
