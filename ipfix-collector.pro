CONFIG += console
CONFIG += warn_on c++11
CONFIG += debug

DESTDIR = build
OBJECTS_DIR = build

QT += network

QMAKE_CXXFLAGS += -Wextra
QMAKE_CXXFLAGS += $$system(pcap-config --cflags)
LIBS += $$system(pcap-config --libs)

HEADERS += src/csv_storage.h
HEADERS += src/dumper.h
HEADERS += src/flow_data.h
HEADERS += src/ipfix_parser.h
HEADERS += src/memory_stream.h
HEADERS += src/null_storage.h
HEADERS += src/options.h
HEADERS += src/pcap_parser.h
HEADERS += src/server.h
HEADERS += src/stdout_storage.h
HEADERS += src/storage.h
HEADERS += src/util.h

SOURCES += src/csv_storage.cpp
SOURCES += src/dumper.cpp
SOURCES += src/flow_data.cpp
SOURCES += src/ipfix_parser.cpp
SOURCES += src/main.cpp
SOURCES += src/memory_stream.cpp
SOURCES += src/null_storage.cpp
SOURCES += src/options.cpp
SOURCES += src/pcap_parser.cpp
SOURCES += src/server.cpp
SOURCES += src/stdout_storage.cpp
SOURCES += src/storage.cpp
SOURCES += src/util.cpp

# Local Variables:
# mode: text
# comment-start: "#"
# End:
